﻿using System;
using AutoMapper;

namespace automapper.console.Steps.InlineMapping
{
    public class InlineMapping
    {
        public static void Perform()
        {
            var source = new Source();

            var dxxest = Mapper.Map<Source, Dest>(source, opt => opt.ConfigureMap()
                                                .ForMember(dest => dest.Value, m => m.MapFrom(src => src.Value + 10)));
        }
    }

    public class Source
    {
        public int Value { get; set; }
    }

    public class Dest
    {
        public int Value { get; set; }
    }
}
