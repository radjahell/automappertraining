﻿using System;
using AutoMapper;

namespace automapper.console.Steps.StaticInstanceApi
{
    public static class StaticInstanceApi
    {
        public static void Perform(){

            var config = new MapperConfiguration(cfg => { 
                cfg.CreateMap<CalendarEntry, CalendryEntryDto>();
            });

            var mapper = config.CreateMapper();
            // or
           // IMapper mapper = new Mapper(config);
            var dest = mapper.Map<CalendarEntry, CalendryEntryDto>(new CalendarEntry());
        }
    }

    public class CalendarEntry
    {
        public DateTime StartDate { get; set; }
        public string EventName { get; set; }
    }

    public class CalendryEntryDto
    {
        public string StartDate { get; set; }
        public string EventName { get; set; }
    }
}
