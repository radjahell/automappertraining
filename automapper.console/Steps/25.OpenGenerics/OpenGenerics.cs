﻿using System;
using automapper.console.Steps.Extensions;
using AutoMapper;

namespace automapper.console.Steps.OpenGenerics
{
    public static class OpenGenerics
    {


        public static void Perform()
        {
            Mapper.Initialize(cfg => cfg.CreateMap(typeof(Source<>), typeof(Destination<>)));

            var source = new Source<int> { Value = 10 };

            var dest = Mapper.Map<Source<int>, Destination<int>>(source);

            dest.Value.ShouldEquals(10);
        }

      
    }

    public class Source<T>
    {
        public T Value { get; set; }
    }

    public class Destination<T>
    {
        public T Value { get; set; }
    }
}
