﻿using automapper.console.Steps.Extensions;
using AutoMapper;

namespace automapper.console.Steps.NullSubstitution
{
    public static class NullSubstitution
    {
        public static void Perform()
        { 
            Mapper.Initialize(cfg =>
              cfg.CreateMap<CalendarEntry, CalendarEntryDto>()
                              .ForMember(destination => destination.EventName,
                                         opt => opt.NullSubstitute("not null")));  

            var source = new CalendarEntry { EventName = null }; 
            var dest = Mapper.Map<CalendarEntry, CalendarEntryDto>(source);

            dest.EventName.ShouldEquals("not null");

            source.EventName = "Not null";

            dest = Mapper.Map<CalendarEntry, CalendarEntryDto>(source);

            dest.EventName.ShouldEquals("Not null");
        }
    }


    public class CalendarEntry
    {
        public string EventName { get; set; } 
    }

    public class CalendarEntryDto
    {   
        public string EventName { get; set; }
    }
}
