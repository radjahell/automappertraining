﻿using System;
using automapper.console.Steps.Extensions;
using AutoMapper;

namespace automapper.console.Steps.ValueConverter
{
    public static class ValueConverter
    {
        public static void Perform()
        {
            var calendarEntry = new CalendarEntry
            {
                StartDate = new DateTime(2018, 1, 1, 1, 1, 1),
                EventName = "Backlog brooming"
            };


            Mapper.Initialize(cfg => 
                cfg.CreateMap<CalendarEntry, CalendryEntryDto>()
                              .ForMember(d => d.StartDate,
                                         opt => opt.ConvertUsing(new DateValueConverter())));

            var calendarEntryDto = Mapper.Map<CalendryEntryDto>(calendarEntry);
            calendarEntryDto.StartDate.ShouldEquals("01/01/2018 01:01 AM");
            calendarEntryDto.EventName.ShouldEquals("Backlog brooming");
         }
    }

    public class DateValueConverter : IValueConverter<DateTime, string>
    {
        public string Convert(DateTime sourceMember, ResolutionContext context) 
                                => sourceMember.ToString("MM/dd/yyyy hh:mm tt");
    }

    public class CalendarEntry
    {
        public DateTime StartDate { get; set; }
        public string EventName { get; set; } 
    }

    public class CalendryEntryDto
    {
        public string StartDate { get; set; }
        public string EventName { get; set; } 
    }
}
