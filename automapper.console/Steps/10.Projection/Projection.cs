﻿using System;
using automapper.console.Steps.Extensions;
using AutoMapper;

namespace automapper.console.Steps.Projection
{
    public static class Projection
    {
        public static void Perform()
        {
            var @class = new Class
            {
                StartDate = new DateTime(2018, 12, 15,6,15,0),
                Title = "Czech Language"
            };

            Mapper.Initialize(cfg =>
              cfg.CreateMap<Class, ClassForm>()
                .ForMember(dest => dest.StartDate, opt => opt.MapFrom(src => src.StartDate.Date))
                .ForMember(dest => dest.Hour, opt => opt.MapFrom(src => src.StartDate.Hour))
                .ForMember(dest => dest.Minute, opt => opt.MapFrom(src => src.StartDate.Minute)));


            ClassForm form = Mapper.Map<Class, ClassForm>(@class);

            form.StartDate.ShouldEquals(new DateTime(2018, 12, 15, 6, 15, 0));
            form.Hour.ShouldEquals(6);
            form.Minute.ShouldEquals(15);
            form.Title.ShouldEquals("Czech Language");

        }
    }


    public class Class
    {
        public DateTime StartDate { get; set; }
        public string Title { get; set; }
    }

    public class ClassForm
    {
        public DateTime StartDate { get; set; }
        public int Hour { get; set; }
        public int Minute { get; set; }
        public string Title { get; set; }
    }

}
