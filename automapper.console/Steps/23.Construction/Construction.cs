﻿using System;
using AutoMapper;

namespace automapper.console.Steps.Construction
{
    public static class Construction
    {
        public static void Perform()
        { 
            Mapper.Initialize(cfg =>
            { 
                cfg.CreateMap<Source, SourceDto>();
                cfg.CreateMap<SourceDiffConst, SourceDtoDiffConst>()
                    .ForCtorParam("valueParamSomeOtherName", opt => opt.MapFrom(src => src.Value)); 
            });

            var source = Mapper.Map<SourceDto>(new Source{ Value = 1 }); 

            var sourcediffconst = Mapper.Map<SourceDtoDiffConst>(new SourceDiffConst { Value = 2 }); 
        }
    }

    public class Source
    {
        public int Value { get; set; }
    }
    public class SourceDto
    {
        public SourceDto(int value)
        {
            _value = value;
        }
        private int _value;
        public int Value
        {
            get { return _value; }
        }
    }

    public class SourceDiffConst
    {
        public int Value { get; set; }
    }
    public class SourceDtoDiffConst
    {
        public SourceDtoDiffConst(int valueParamSomeOtherName)
        {
            _value = valueParamSomeOtherName;
        }
        private int _value;
        public int Value
        {
            get { return _value; }
        }
    }
}
