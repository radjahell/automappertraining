﻿using System;
using automapper.console.Steps.Extensions;
using AutoMapper;

namespace automapper.console.Steps.ValueTransformers
{
    public class ValueTransformers
    {
        public static void Perform()
        {
            Mapper.Initialize(cfg => {
                cfg.ValueTransformers.Add<string>(val => val + "!!!");
            });

            var source = new Source { Value = "Hello" };
            var dest = Mapper.Map<Destination>(source);

            dest.Value.ShouldEquals("Hello!!!"); 
        }
    }

    public class Source
    {
        public string Value { get; set; } 
    }

    public class Destination
    {
        public string Value { get; set; }
    }
}
