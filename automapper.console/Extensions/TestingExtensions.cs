﻿using System;
namespace automapper.console.Steps.Extensions
{
    public static class TestingExtensions
    {

        public static void ShouldEquals<T>(this T source, T destination) where T : IEquatable<T>
        {

            if (source.Equals(destination))
            { 
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"source: {source} equals to the destination: {destination}");
            }
            else
            { 
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"source: {source} NOT equals to the destination: {destination}");
            }
        } 
    }
}
